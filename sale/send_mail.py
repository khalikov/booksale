import smtplib

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


def send_mail(message, topic, to):
    try:
        from . import mail_params
    except ImportError:
        print('MAIL WAS NOT SENT DUE TO NO "mail_params.py" file in "sale" module')
        return

    msg = MIMEMultipart()
    msg['From'] = mail_params.addr_from
    msg['To'] = to
    msg['Subject'] = topic

    msg.attach(MIMEText(message, 'plain'))

    server = smtplib.SMTP(mail_params.server_name, mail_params.tls_port)
    server.starttls()
    server.login(mail_params.addr_from, mail_params.password)
    server.send_message(msg)
    server.quit()
