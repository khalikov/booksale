import json
import traceback

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseBadRequest, HttpResponse
from django.views import View

from sale.models import BookUser, Author, Book, BookBuyRequest


class AuthMixin:
    @classmethod
    def as_view(cls, **kwargs):
        view = super().as_view(**kwargs)
        return login_required(view)


class ViewWithAuthMixin(AuthMixin, View):
    pass


class RegistrationView(View):
    def post(self, request):
        try:
            post = json.loads(request.body)
        except Exception:
            return HttpResponseBadRequest
        username = post.get('username')
        password = post.get('password')
        if username and password:
            try:
                if BookUser.objects.filter(username=username).exists():
                    return HttpResponseBadRequest('')
                BookUser.objects.create_user(username,
                                             password=password)
                return HttpResponse(b'{"status": "OK"}')
            except Exception as e:
                print(traceback.format_exc())
                # я не знаю, какие точно исключения в будущем
        return HttpResponseBadRequest('')


class AuthorizationView(View):
    def post(self, request):
        try:
            post = json.loads(request.body)
        except Exception:
            return HttpResponseBadRequest
        username = post.get('username')
        password = post.get('password')

        if username and password:
            try:
                user = authenticate(request,
                                    username=username,
                                    password=password)
                if user is not None:
                    login(request, user)
                return HttpResponse(b'{"status": "OK"}')
            except Exception:
                pass
                # я не знаю, какие точно исключения в будущем
        return HttpResponseBadRequest('')

    def delete(self, request):
        if request.user.is_authenticated:
            logout(request)
        return HttpResponse(b'{"status": "OK"}')


class AuthorsView(ViewWithAuthMixin):
    def get(self, request):
        authors = Author.objects.prefetch_related(
            'books'
        )
        auths = []
        for author in authors:
            obj = {
                'id': author.id,
                'fio': author.fio,
                'books': [],
                'books_count': 0
            }
            books = author.books.all()
            obj['books_count'] = books.count()
            for book in books:
                obj['books'].append({
                    'book_id': book.id,
                    'book_name': book.name,
                    'book_price': str(book.price)
                })
            auths.append(obj)
        result = {
            "status": "OK",
            "authors": auths
        }
        return HttpResponse(json.dumps(result))


class BooksView(ViewWithAuthMixin):
    def get(self, request):
        bks = []
        books = Book.objects.select_related('author')
        for book in books:
            bks.append({
                'id': book.id,
                'name': book.name,
                'author_fio': book.author.fio,
                'price': str(book.price)
            })
        return HttpResponse(json.dumps({'status': "OK", "books": bks}))


class BuyRequestView(ViewWithAuthMixin):
    def post(self, request):
        try:
            post = json.loads(request.body)
        except Exception:
            return HttpResponseBadRequest('')
        phone = post.get('buyer_phone')
        if not phone:
            return HttpResponseBadRequest('')
        if not request.user.is_authenticated:
            return HttpResponseBadRequest('')
        comment = request.POST.get('comment')
        if not comment:
            comment = ''

        req = BookBuyRequest(user_id=request.user.id,
                             buyer_phone=phone,
                             comment=comment)
        req.save()
        from sale.send_mail import send_mail
        message = '''
Была создана заявка для пользователя {id}
с телефоном {phone} и комментарием "{comment}"
        '''.format(id=request.user.id, phone=phone, comment=comment)
        topic = 'Новая заявка'
        for u in BookUser.objects.filter(is_staff=True):
            if u.email:
                send_mail(message, topic, u.email)
        print('done buy request save')

        return HttpResponse(b'{"status": "OK"}')