from django.urls import path

from sale.views import RegistrationView, AuthorizationView, AuthorsView, BooksView, BuyRequestView

urlpatterns = [
    path(r'registration', RegistrationView.as_view()),
    path(r'authorization', AuthorizationView.as_view()),
    path(r'authors', AuthorsView.as_view()),
    path(r'books', BooksView.as_view()),
    path(r'request', BuyRequestView.as_view())
]