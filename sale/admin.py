from django.contrib import admin
from sale.models import BookUser, Book, Author, BookBuyRequest


class AuthorAdmin(admin.ModelAdmin):
    pass


class BookUserAdmin(admin.ModelAdmin):
    pass


class BookAdmin(admin.ModelAdmin):
    pass


class BookBuyRequestAdmin(admin.ModelAdmin):
    pass


admin.site.register(Author, AuthorAdmin)
admin.site.register(BookUser, BookUserAdmin)
admin.site.register(Book, BookAdmin)
admin.site.register(BookBuyRequest, BookBuyRequestAdmin)
